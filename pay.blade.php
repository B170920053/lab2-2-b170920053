



<!doctype html>
<html>
<head>
    
<title> ТӨЛБӨР</title>
</head>
<link href="css/pay.css" rel="stylesheet"/>
<body>


    
        @include('includes.header')
   

  <section id="services2">
<div class="container">
<h1>Төлбөрийн Багц</h1>
</div>  
  </section>


<!-----  Price plans----->


<section id="price">
<div class="container">
<h5>Хичээл эхлэхээс 6 цагийн өмнө цагаа захиалах ёстой ! </h5>

<div class="row">

<div class="col-md-3">
<div class="single-price">
<div class="price-head">
<h2>Туршилт</h2>
<p>18000₮/ <span>3 хоног</span></p>
</div>
<div class="price-content">
<ul>
<li> <i class="fa fa-check-circle"></i> 1 хичээл  </li>
<li> <i class="fa fa-check-circle"></i>18 0000₮ </li>
<li> <i class="fa fa-times-circle"></i> Хичээл цуцлах  </li>
<li>  <i class="fa fa-times-circle"></i> Сурах бичиг   </li>
<li>  <i class="fa fa-times-circle"></i> Үдийн цай   </li>

</ul>
</div>
<div class="price-button">
<a class="buy-btn" href="#"> Сонгох </a>
</div>
</div>
</div>

<div class="col-md-3">
<div class="single-price">
<div class="price-head">
<h2>1 Сар</h2>
<p>216000₮/ <span>30 хоног</span></p>
</div>
<div class="price-content">
<ul>
<li> <i class="fa fa-check-circle"></i> 12 хичээл   </li>
<li> <i class="fa fa-check-circle"></i> 216 000₮ </li>
<li> <i class="fa fa-check-circle"></i> Хичээл цуцлах   </li>
<li>  <i class="fa fa-times-circle"></i> Сурах бичиг   </li>
<li>  <i class="fa fa-times-circle"></i> Үдийн цай   </li>

</ul>
</div>
<div class="price-button">
<a class="buy-btn" href="#"> Сонгох </a>
</div>
</div>
</div>


<div class="col-md-3">
<div class="single-price">
<div class="price-head">
<h2>2 Сар</h2>
<p>$390000₮/ <span>60 хоног</span></p>
</div>
<div class="price-content">
<ul>
<li> <i class="fa fa-check-circle"></i> 24 хичээл   </li>
<li> <i class="fa fa-check-circle"></i> 390000₮ </li>
<li> <i class="fa fa-check-circle"></i>  Хичээл цуцлах   </li>
<li>  <i class="fa fa-check-circle"></i> Сурах бичиг   </li>
<li>  <i class="fa fa-times-circle"></i> Үдийн цай   </li>

</ul>
</div>
<div class="price-button">
<a class="buy-btn" href="#"> Сонгох </a>
</div>
</div>
</div>



<div class="col-md-3">
<div class="single-price">
<div class="price-head">
<h2>3 Сар</h2>
<p>585000₮/ <span>90 хоног</span></p>
</div>
<div class="price-content">
<ul>
<li> <i class="fa fa-check-circle"></i> 36 хичээл   </li>
<li> <i class="fa fa-check-circle"></i> 585000₮ </li>
<li> <i class="fa fa-check-circle"></i> Хичээл цуцлах   </li>
<li>  <i class="fa fa-check-circle"></i>Сурах бичиг   </li>
<li>  <i class="fa fa-check-circle"></i> Үдийн цай   </li>

</ul>
</div>
<div class="price-button">
<a class="buy-btn" href="#"> Сонгох </a>
</div>
</div>
</div>


</div>
</div>

</section>





   
        @include('includes.footer')
  


</body>
</html>


