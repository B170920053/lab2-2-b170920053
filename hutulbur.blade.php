
<!doctype html>
<html>
<head>
   

<title> 
ХӨТӨЛБӨР
</title>
    <link href="css/hutulbur.css" rel="stylesheet"/>
</head>
<body>


   
        @include('includes.header')
 


        <section id="services3">
<div class="container">
<h1>  Хөтөлбөр </h1>
</div>  
  </section>

  <div class="fast">
<h3> МАНАЙ СУРГАЛТЫН ОНЦЛОГ</h3>
<br>

<div class="container">
<div class="row">

<div class="col-md-4">
<img src="/img/fan.svg" alt="">
<p>Хичээл амьд харилцан яриа
хэлбэрээр явагддаг.</p>



</div>

<div class="col-md-4">
<img src="/img/uk.svg" alt="">
<p> Хичээл дан Англи хэл
дээр явагдана.</p>
</div>




<div class="col-md-4">
<img src="/img/classroom.svg" alt="">
<p> Та эхний хичээлээс эхлэн
Англиар ярьж эхэлнэ.  </p>
</div>


</div>
</div>
</div>



<div class="container">
<div class="row">
<div class="col-md-12">

<p> 

Гадаад хэл сурахын гол зорилго бол тест бөглөж сурахад биш 
тухайн хэлээр бусадтай харилцаж сурахад оршдог.  
Тийм учраас манай бүх хөтөлбөрүүд CLT (Communicative Language teaching) буюу 
тухайн суралцаж байгаа хүнийхээ бодит хэрэгцээнд тулгуурлан унаган Анги хэлтэй гадаад 
багш нартай амьд харилцаа өрнүүлэх замаар тухайн суралцагчийн Англи хэлээр өөрийгээ 
илэрхийлэх бусдыг ойлгох чадварыг хөгжүүлэхэд чиглэдэг юм.
<br>


<h4 class="text-center"> БИД ТАНД ДАРААХ 4 ХӨТӨЛБӨРИЙГ САНАЛ БОЛГОЖ БАЙНА!  </h4>
<br>
</p>
</div>
</div>
</div>


<div class="row text-center">
                <div class="col-md-3 topMargin">
                        <div class="panel panel-default">
                            <div class="panel-heading">ЕРӨНХИЙ ЯРИА</div>
                            <img src="img/index.png">
                            <div class="panel-body">
                                <p class="courseIntro">Мэндлэх, өөрийгээ танилцуулах, өөрт болон бусдад хэрэгтэй байгаа зүйлсийн талаар тайлбарлах, асуух, өөрийн сэтгэл санааны байдал, үзэл бодлоо илэрхийлэх, юмс үзэгдлийг дүрслэн илэрхийлэх гэх мэтчилэн та бидний өдөр тутмын ярианы чадварыг олгох хөтөлбөр юм.</p>
                                <a class="btn btn-warning btn-block tag" >
                                        МАТЕРИАЛУУД                                    
                                </a>
                            </div>
                        </div>
                </div>
                <div class="col-md-3 topMargin">
                        <div class="panel panel-default">
                            <div class="panel-heading">БИЗНЕСИЙН ЯРИА</div>
                            <img src="img/student2.png">
                            <div class="panel-body">
                                <p class="courseIntro">Гадны түншүүдтэй харилцах, бүтээгдэхүүн үйлчилгээгээ танилцуулах, утас мэйл-нд хариулах, тайлан тавих,танилцуулга хийх, хэлэлцээр хийх, өөрийн санааг хамгаалах зэрэг ажил хэргийн хүрээнд хэрэглэгдэх ярианы чадварыг олгох хөтөлбөр юм.</p>
                                <a class="btn btn-warning btn-block tag" >
                                        МАТЕРИАЛУУД                                    
                                </a>                                
                            </div>
                        </div>
                </div>
                <div class="col-md-3 topMargin">
                        <div class="panel panel-default">
                            <div class="panel-heading">TOEFL/IELTS-ИЙН ЯРИА</div>
                            <img src="img/student3.png">
                            <div class="panel-body">

                                <p class="courseIntro">TOEFL болон IELTS-ийн ярианы хэсэгт ирдэг асуулт, даалгаваруудыг дуурайн тус шалгалтуудад заагдсан хугацаанд багтаан оюутнаар хариулуулж оюутны гаргасан дүрмийн болон утгын алдааг залруулж өгөх замаар TOEFL IELTS-ийн ярианы хэсэгт бэлдэх сургалт.</p>
                                <a class="btn btn-warning btn-block tag" >
                                        МАТЕРИАЛУУД                                    
                                </a>
                            </div>
                        </div>
                </div>
                <div class="col-md-3 topMargin">
                        <div class="panel panel-default">
                            <div class="panel-heading">ЯРИЛЦЛАГАНД БЭЛДЭХ</div>
                            <img src="img/student4.png">
                            <div class="panel-body">
                                <p class="courseIntro">Элчийнгийн ярилцлага, ажилд орох ярилцлага, гадагшаа сургуульд явах ярилцлага зэрэгт түгээмэл ирдэг асуултуудын дагуу асуулт хариулт хэлбэрээр ярианы дадлага олгох сургалт</p>
                                <a class="btn btn-warning btn-block tag"">
                                        МАТЕРИАЛУУД                                    
                                </a>                                
                            </div>
                        </div>
                </div>
        </div>

   


        @include('includes.footer')



</body>
</html>