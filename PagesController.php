<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
            {
    //
    public function getHome() 
    {
    return View ('pages.home');
    }

public function getHutulbur() 
{
return View ('pages.hutulbur');
}

public function getTeachers() 
{
    return View ('pages.teachers');
}


public function getLevel() 
{
    return View ('pages.level');
}


public function getHelp()
{
        return View ('pages.help');
}



public function getPay() 
{
    return View ('pages.pay');
}


}
