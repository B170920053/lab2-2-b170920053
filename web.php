<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',        'PagesController@getHome');
Route::get('level',    'PagesController@getLevel' );
Route::get('teachers', 'PagesController@getTeachers');
Route::get('pay',      'PagesController@getPay' );
Route::get('hutulbur', 'PagesController@getHutulbur' );
Route::get('help',     'PagesController@getHelp' );