
<!doctype html>
<html>
<head>
     <title> Тусламж </title>
    <link href="css/help.css" rel="stylesheet"/>
</head>
<body>


   
        @include('includes.header')
 


        <section id="services3">
<div class="container">
<h1>Тусламж</h1>
</div>  

  </section>
   

  <div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h2>СУРГАЛТЫН ХӨТӨЛБӨР</h2>
				<p>Гадаад хэл сурна гэдэг нь тухайн хэлээр харилцаж сурахыг хэлэхээс
                     биш тухайн хэлний талаарх салангид дүрэм, үгсийн 
                    сангийн мэдлэгтэй болж тест бөглөж сурахыг хэлэхгүй юм.
                     Тийм учраас бид Англи хэлийг заахдаа суралцагчийнхаа
                      Англи хэлээр харилцаж сурахад нь түлхүү анхаарч буюу “
                      Communicative language teaching” зарчимыг баримталдаг. </p>
				<div class="row">
					<div class="col-md-12">
						<img class="img-fluid" src="img/tt2.png">
					</div>
				</div>
			</div>
		</div>
	</div>



  <div class="container partner">
		<div class="row">
			<div class="col-md-7 text-center">
				<h5 style="color: #007BB9;">Манай сургалтанд төрөл бүрийн ажил мэргэжлийн хүмүүс хамрагддаг, тухайлбал:</h5>
				<br>
                                
                                <img class="img-fluid" src="img/tt.png" style="max-height:100%">                               
                                
			</div>
			<div class="col-md-4">
				<!--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fenglish.maestro.mn&tabs=timeline&width=500px&height=330px&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=1151649211568700" width="95%" height="330px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>-->
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fenglish.maestro.mn%2F&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=223072431052910" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>
		</div>
		
	</div>

        @include('includes.footer')

</body>
</html>


